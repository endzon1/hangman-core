const pickFromArray = require('./pick-random-from-array');
const getStateFromHistory = require('./get-state-from-history');

module.exports = function createHangman(words, time) {
  let history, word;

  const initialize = () => {
    word = pickFromArray(words);
    history = [];
  }

  const play = letter => {
    if (letter) { history.push(letter); }
    return getStateFromHistory(word, history, time);
  }

  initialize();
  
  const counterId = setInterval(() => { 
    time -= 1000;
  
    if(time <= 0){
      clearInterval(counterId);
    }
  }, 1000);

  return { play, reset: initialize };
}