const getCurrentWordFromHistory = require('./get-current-word-from-history');
const getErrorsFromHistory = require('./get-errors-from-history');
const getAscii = require('./get-ascii');
const getScore = require('./get-score');

module.exports = function getStateFromHistory(word, history, time) {
  return {
    ascii: getAscii(getErrorsFromHistory(word, history)),
    numberOfLetters: word.replace(/\s/g, '').length,
    currentState: getCurrentWordFromHistory(word, history),
    errorsNumber: getErrorsFromHistory(word, history),
    score: getScore(word, history),
    history,
    word,
    time
  }
}
