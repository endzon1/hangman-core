module.exports = function replacePositions(word, positions, letter) {
  return positions.reduce((prev, position) => (
    `${prev.slice(0, position)}${letter}${prev.slice(position + 1)}`
  ), word)
}
